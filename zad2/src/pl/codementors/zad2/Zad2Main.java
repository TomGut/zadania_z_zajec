package pl.codementors.zad2;

import java.util.Scanner;

public class Zad2Main
{
    public static void main(String[] args)
    {
        System.out.println("Hey");

        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Podaj proszę liczbę do obliczenia silni");
        
        long number = inputScanner.nextInt();

        if(number <= 1){

            System.out.println("Musisz podać liczbę dodatnią, większą niż 0");
        }

        else {
            long silnia = number;

            for(int i=1; i<number; i++){

                silnia = silnia * i;
            }

            System.out.println("silnia z: " + number + " ,wynosi: " + silnia);
        }
    }
}