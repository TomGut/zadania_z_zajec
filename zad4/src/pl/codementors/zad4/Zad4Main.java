package pl.codementors.zad4;

import java.util.Scanner;

public class Zad4Main{
    public static void main(String[] args){

        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Podaj proszę rozmiar tablicy");
        
        int number = inputScanner.nextInt();
        int newArray[] = new int[number];

        if(newArray.length > 0){

            for(int i=0 ; i<newArray.length; i++){

                System.out.println("Podaj proszę indeks nr. : " + i);
                int number2 = inputScanner.nextInt();
                newArray[i] = number2;        
            }

            int tmp = newArray[newArray.length-1]; 

            for(int i=0; i<newArray.length; i++){
                System.out.println("Wartość indeksu tablicy " + i + " przed zmianą " + newArray[i]);
            }

            for(int i=newArray.length -1; i>0; i--){
                newArray[i] = newArray[i-1];
            }

            newArray[0] = tmp;

            for(int i=0; i<newArray.length; i++){
                System.out.println("Wartość indeksu tablicy " + i + " po zmianie " + newArray[i]);
            }
        }
        else{
            System.out.println("Musisz podać przynajmniej 1 wartość indexu tablicy");
        }
    }
}