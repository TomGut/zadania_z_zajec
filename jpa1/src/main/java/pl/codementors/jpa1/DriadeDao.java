package pl.codementors.jpa1;

import pl.codementors.jpa1.Models.Driade;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class DriadeDao {
    public DriadeDao(){
    }

    public Driade persist(Driade driade) {
        EntityManager em = TreeEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(driade);
        tx.commit();
        em.close();
        return driade;
    }

    public Driade merge(Driade driade){
        EntityManager em = TreeEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        driade = em.merge(driade);
        tx.commit();
        em.close();
        return driade;
    }

    public Driade find(int id) {
        EntityManager em = TreeEntityManagerFactory.createEntityManager();
        Driade driade = em.find(Driade.class, id);
        em.close();
        return driade;
    }

    public void delete(Driade driade) {
        EntityManager em = TreeEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(driade));
        tx.commit();
        em.close();
    }

    public List<Driade> findAllCriteriaApi() {
        EntityManager em = TreeEntityManagerFactory.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Driade> query = cb.createQuery(Driade.class);
        query.from(Driade.class);
        List<Driade> driades = em.createQuery(query).getResultList();
        em.close();
        return driades;
    }
}
