package pl.codementors.jpa1;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TreeEntityManagerFactory {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa1PU");

    public static final EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    public static void close() {
        emf.close();
    }
}