package pl.codementors.jpa1;

import pl.codementors.jpa1.Models.Driade;
import pl.codementors.jpa1.Models.Tree;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class TreeDao {

    public TreeDao(){
    }

    public Tree persist(Tree tree) {
        EntityManager em = TreeEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(tree);
        tx.commit();
        em.close();
        return tree;
    }

    public Tree merge(Tree tree){
        EntityManager em = TreeEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        tree = em.merge(tree);
        tx.commit();
        em.close();
        return tree;
    }

    public Tree find(int id) {
        EntityManager em = TreeEntityManagerFactory.createEntityManager();
        Tree tree = em.find(Tree.class, id);
        em.close();
        return tree;
    }

    public void delete(Tree tree) {
        EntityManager em = TreeEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(tree));
        tx.commit();
        em.close();
    }

    public List<Tree> findAllCriteriaApi() {
        EntityManager em = TreeEntityManagerFactory.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tree> query = cb.createQuery(Tree.class);
        query.from(Tree.class);
        List<Tree> trees = em.createQuery(query).getResultList();
        em.close();
        return trees;
    }
}