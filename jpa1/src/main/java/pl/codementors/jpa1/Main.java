package pl.codementors.jpa1;

import pl.codementors.jpa1.Models.Driade;
import pl.codementors.jpa1.Models.Tree;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        boolean isRunning = true;
        String specie;
        String name;
        String user_input;
        int height;
        int power;
        int id;
        TreeDao treeDao = new TreeDao();
        DriadeDao driadeDao = new DriadeDao();
        Scanner scanner = new Scanner(System.in);

        while (isRunning){
            System.out.println(
                    "MENU"
                    + "\n1 - wyjście z programu"
                    + "\n2 - wypisanie wszystkich obiektów w wyborze"
                    + "\n3 - dodaj nowy obiekt"
                    + "\n4 - modyfikacja wybranego obiektu"
                    + "\n5 - usunięcie wybranego obiektu");

            user_input = scanner.next();

            switch (user_input){
                case "1": {
                    TreeEntityManagerFactory.close();
                    System.out.println("Bye !");
                    isRunning = false;
                    break;
                }
                case "2": {
                    System.out.println("Wypisać: \n1 - drzewa\n2 - driady");
                    user_input = scanner.next();
                    if(user_input.equals("1")){
                        System.out.println(treeDao.findAllCriteriaApi());
                    }
                    if(user_input.equals("2")){
                        System.out.println(driadeDao.findAllCriteriaApi());
                    }
                    break;
                }
                case  "3": {
                    System.out.println("Dodanie: \n1 - drzewa\n2 - driady");
                    user_input = scanner.next();
                    if(user_input.equals("1")){
                        System.out.println(
                                "Podaj typ drzewa");
                        specie = scanner.next();
                        System.out.println("Podaj wzrost");
                        height = scanner.nextInt();
                        Tree tree = new Tree(specie, height);
                        treeDao.persist(tree);
                        System.out.println("Dodano drzewo:" + treeDao.find(tree.getId()));
                    }
                    if(user_input.equals("2")){
                        System.out.println(
                                "Podaj imię driady");
                        name = scanner.next();
                        System.out.println("Podaj moc");
                        power = scanner.nextInt();
                        System.out.println("Podaj ID drzewa rodziciela");
                        id = scanner.nextInt();
                        Driade driade = new Driade(name, power, treeDao.find(id));
                        driadeDao.persist(driade);
                        System.out.println(
                                "Dodano driade:"
                                + driadeDao.find(driade.getId())
                                + "\nktórej drzewem rodzicielem jest: "
                                + driadeDao.find(driade.getId()).getTree().getSpecie());
                    }
                    break;
                }
                case "4": {
                    System.out.println("Modyfikacja\n1 - drzewa\n2 - driady");
                    user_input = scanner.next();
                    if(user_input.equals("1")){
                        System.out.println("Modyfikacja wybranego drzewa\nPodaj ID");
                        id = scanner.nextInt();
                        Tree tree = treeDao.merge(treeDao.find(id));
                        System.out.println("Czy chcesz zmienić gatunek ?\n1 - TAK\n2 - NIE");
                        user_input = scanner.next();
                        if(user_input.equals("1")){
                            System.out.println("podaj nowy gatunek drzewa");
                            user_input = scanner.next();
                            tree.setSpecie(user_input);
                            treeDao.merge(tree);
                        }
                        if(user_input.equals("2")){
                            System.out.println("Czy chcesz zmienić wzrost ?\n1 - TAK\n2 - NIE");
                            height = scanner.nextInt();
                            tree.setHeight(height);
                            treeDao.merge(tree);
                        }
                        System.out.println(
                                "Drzewo ma teraz nowe parametry: "
                                + "\nGatunek: "
                                + tree.getSpecie()
                                + "\nWzrost: "
                                + tree.getHeight());
                    }
                    if(user_input.equals("2")){
                        System.out.println("Modyfikacja wybranej driady\nPodaj ID");
                        id = scanner.nextInt();
                        Driade driade = driadeDao.merge(driadeDao.find(id));
                        System.out.println("Czy chcesz zmienić imię ?\n1 - TAK\n2 - NIE");
                        user_input = scanner.next();
                        if(user_input.equals("1")){
                            System.out.println("podaj nowe imię driady");
                            user_input = scanner.next();
                            driade.setName(user_input);
                            driadeDao.merge(driade);
                        }

                        System.out.println("Czy chcesz zmienić moc ?\n1 - TAK\n2 - NIE");
                        user_input = scanner.next();
                        if(user_input.equals("1")){
                            user_input = scanner.next();
                            System.out.println("Podaj nową wartośc mocy");
                            power = scanner.nextInt();
                            driade.setPower(power);
                            driadeDao.merge(driade);
                            }

                        System.out.println("Czy chcesz zmienić drzewo rodziciela ? \n1 - TAK\n2 - NIE");
                        user_input = scanner.next();
                        if(user_input.equals("1")){
                                System.out.println("Podaj nowe ID drzewa");
                                id = scanner.nextInt();
                                driade.setTree(treeDao.find(id));
                            }

                        System.out.println(
                                "Driada ma teraz nowe parametry: "
                                + "\nImię: "
                                + driade.getName()
                                + "\nMoc: "
                                + driade.getPower()
                                + "\nDrzewo rodziciel "
                                + driade.getTree().getSpecie());
                        }
                    break;
                }
                case "5": {
                    System.out.println(
                            "Usunięcie"
                            + "\n1 - drzewa"
                            + "\n2 - driady");
                    user_input = scanner.next();
                    if(user_input.equals("1")){
                        System.out.println("Usunięcie wybranego drzewa\nPodaj ID");
                        id = scanner.nextInt();
                        System.out.println(
                                "Drzewo"
                                + treeDao.find(id)
                                + "do usunięcia ?"
                                + "\n1 - TAK"
                                + "\n2 - NIE");
                        user_input = scanner.next();
                        if(user_input.equals("1")){
                            treeDao.delete(treeDao.find(id));
                        }
                    }
                    if(user_input.equals("2")){
                        System.out.println("Usunięcie wybranej driady\nPodaj ID");
                        id = scanner.nextInt();
                        System.out.println(
                                "Driada"
                                        + driadeDao.find(id)
                                        + "do usunięcia ?"
                                        + "\n1 - TAK"
                                        + "\n2 - NIE");
                        user_input = scanner.next();
                        if(user_input.equals("1")){
                            driadeDao.delete(driadeDao.find(id));
                        }
                    }
                    break;
                }
            }
        }
    }
}
