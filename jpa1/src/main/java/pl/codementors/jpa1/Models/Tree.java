package pl.codementors.jpa1.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "trees")
public class Tree {
    @Column
    private String specie;

    @Column
    private int height;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public Tree(){
    }

    public Tree(String specie, int height) {
        this.specie = specie;
        this.height = height;
    }

    public String getSpecie() {
        return specie;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Tree\nspecie="
                + specie
                + "\nheight="
                + height
                + "\nid="
                + id;
    }
}
