package pl.codementors.jpa1.Models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "driades")
public class Driade {
    private String name;
    private int power;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade =  CascadeType.ALL)
    @JoinColumn(name = "parent_tree", referencedColumnName = "id")
    private Tree tree;

    public Driade(){
    }

    public Driade(String name, int power, Tree tree) {
        this.name = name;
        this.power = power;
        this.tree = tree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Driade{"
                + "name='"
                + name + '\''
                + ", power="
                + power
                + ", id="
                + id
                + ", tree="
                + (tree != null ? tree.getSpecie() : null);
    }
}
