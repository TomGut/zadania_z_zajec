package pl.codementors.Models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tree.class)
public abstract class Tree_ {

	public static volatile SingularAttribute<Tree, String> specie;
	public static volatile SingularAttribute<Tree, Integer> id;
	public static volatile SingularAttribute<Tree, Integer> height;

}

