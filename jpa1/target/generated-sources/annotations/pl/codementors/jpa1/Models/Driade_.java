package pl.codementors.jpa1.Models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Driade.class)
public abstract class Driade_ {

	public static volatile SingularAttribute<Driade, String> name;
	public static volatile SingularAttribute<Driade, Tree> tree;
	public static volatile SingularAttribute<Driade, Integer> power;
	public static volatile SingularAttribute<Driade, Integer> id;

}

