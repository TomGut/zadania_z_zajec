package pl.codementors.zad3;

import java.util.Scanner;

public class Zad3Main{
    public static void main(String[] args){

        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Podaj proszę rozmiar tablicy");
        
        int number = inputScanner.nextInt();
        int newArray[] = new int[number];

        System.out.println(" ");

        if(newArray.length > 0){

            int suma = 0;
            int iloczyn = 1;

            for(int i=0 ; i<newArray.length; i++){

                System.out.println("Podaj proszę indeks nr. : " + i);
                int number2 = inputScanner.nextInt();
                newArray[i] = number2;

                iloczyn = iloczyn * newArray[i];
                suma = suma + newArray[i];
            }

            System.out.println(" ");
            System.out.println("Tablica ma " + newArray.length + " indeksy");
            System.out.println(" ");
            System.out.println("Iloczyn wszystkich indeksów tablicy to: " + iloczyn);
            System.out.println("Suma wszystkich indeksów tablicy to: " + suma);
            System.out.println(" ");
        }
        else{
            System.out.println("Musisz podać przynajmniej 1 wartość indeksu tablicy");
        }
    }
}