package pl.codementors;

public class ArrayOfInts {
    //tworzymy nullową tablicę
    private int[] tab;

    /*
    ten zapis int... umożliwia podanie dowolnej ilości zmiennych bez konstruowania
    następnych konstruktorów wartości te są przechowywane w zmiennej values
     */

    public ArrayOfInts(int... values){
        tab = values;
    }

    //dodajemy customowy wyjątek
    public int get(int idx) throws WrongIndexException{

        if(idx >= 0 || idx <= tab.length){
            return tab[idx];
        }else{
            String message = "Zły index tablicy, podałeś" + idx + " a tablica ma " + tab.length + " indeksów";
            throw new WrongIndexException(message);
        }
    }

    public void set(int idx, int value){
        tab[idx] = value;
    }

    public void printAll(){
        for(int i=0; i<tab.length; i++){
            System.out.println(tab[i]);
        }
    }

}
