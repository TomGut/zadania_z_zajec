package pl.codementors.zad1;

import java.util.Scanner;

public class Zad1Main
{
    public static void main(String[] args)
    {
        System.out.println("Hey");

        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Podaj proszę pierwszą liczbę");
        int firstNumber = inputScanner.nextInt();

        System.out.println("Podaj proszę drugą liczbę");
        int secondNumber = inputScanner.nextInt();

        if (firstNumber<secondNumber){
        
            for(int i=firstNumber; i<=secondNumber; i++){
                System.out.println(i + " ");
            }
        }
        else if (firstNumber>secondNumber){

            for(int i=firstNumber; i>secondNumber; i--){
                System.out.println(i + " ");
            }
        }
    }

}